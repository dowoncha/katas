// http://codekata.com/kata/kata02-karate-chop/

// Iterative binary search
// Assume values are sorted

fn bin_search(x: i32, values: &[i32]) -> Option<usize> {
    let mut low = 0;
    let mut high = values.len();
    let mut mid = ( low + high ) / 2;

    loop {
        if (low == high && values[low] != x) {
            return None;
        }

        if (x == values[mid]) {
            return Some(mid);
        } else if (x < values[mid]) {
            high = mid;
        } else if (x > values[mid]) {
            low = mid;
        }
    }
}

// bin search 2

// 3

// 4
// 5

fn main() {
    let x = 5;
    let values = vec![0, 3, 4, 4, 5, 19, 2000, 234542];

    assert_eq!(bin_search(x, &values), Some(4));
}