/**
 * Binary Search Tree
 * 
 * BST Property - Key in each node must be greater than or equal to any key stored in the left sub tree and <= to any key stored in the right sub tree
 * Leaves of the tree contain no key and no structure
 */

struct BinaryTreeNode<K, V> {
    key: K,
    value: V,
    left: Option<Box<BinaryTreeNode<K, V>>>,
    right: Option<Box<BinaryTreeNode<K, V>>>
}

/**
 * Recursive binary tree search implementation
 */
fn binary_tree_search<K, V>(key: K, node: Option<&Box<BinaryTreeNode<K, V>>>) -> Option<&Box<BinaryTreeNode<K, V>>> 
    where K: std::cmp::PartialEq + std::cmp::PartialOrd
{
    if node.is_none() || node.unwrap().key == key {
        return node;
    }

    if key < node.unwrap().key {
        return binary_tree_search(key, node.unwrap().left.as_ref());
    } 

    return binary_tree_search(key, node.unwrap().right.as_ref());
}

/**
 * Recursive binary tree insert
 */
// fn recursive_binary_tree_insert<K, V>(root: &mut Option<&Box<BinaryTreeNode<K, V>>>, key: K, value: V)
//     where K: std::cmp::PartialEq + std::cmp::PartialOrd
// {
//     if root.is_none() {
//         let new_root = Box::new(BinaryTreeNode { key: key, value: value, left: None, right: None });
//         *root = Some(&new_root);
//     } else if let Some(root) = root {
//         if root.key == key {
//             root.value = value
//         } else if key < root.key {
//             recursive_binary_tree_insert(&mut root.left.as_ref(), key, value);
//         } else {
//             recursive_binary_tree_insert(&mut root.right.as_ref(), key, value);
//         }
//     }
// }
