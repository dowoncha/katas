extern crate rand;

extern crate nalgebra;
extern crate nphysics2d;
extern crate ncollide2d;

use rand::{Rng, thread_rng};

use nalgebra::{Vector2};
use ncollide2d::shape::{self, ShapeHandle, Ball, Cuboid};
use nphysics2d::world::{World};
use nphysics2d::object::{RigidBodyDesc, ColliderDesc};

struct Rect {
    x1: i32,
    y1: i32,
    x2: i32,
    y2: i32
}

impl Rect {
    pub fn intersects(&self, other: &Rect) -> bool {
        if self.x2 < other.x1 || self.x1 > other.x2 {
            return false
        }
        if self.y2 < other.y1 || self.y1 > other.y2 {
            return false
        }

        true
    }
}

fn gen_rooms(map_width: i32, map_height: i32, min_room_size: i32, max_room_size: i32, room_count: i32) -> Vec<Rect> {
    let mut rng = thread_rng();

    let mut rooms = vec![];

    for _ in 0..room_count {
        let room_x = rng.gen_range(0, map_width - max_room_size);
        let room_y = rng.gen_range(0, map_height - max_room_size);

        let room_width = rng.gen_range(min_room_size, max_room_size);
        let room_height = rng.gen_range(min_room_size, max_room_size);

        rooms.push(Rect { x1: room_x, y1: room_y, x2: room_x + room_width, y2: room_y + room_height });
    }

    rooms
}

/**
 * Impulse resolution - A collision resolution strategy
 * Modify two intersecting objects to not allow them to remain intersecting
 */

fn main() {
    let map_width = 200;
    let map_height = 200;

    let min_room_size = 4;
    let max_room_size = 20;

    let room_count = 20;

    let rooms = gen_rooms(map_width, map_height, min_room_size, max_room_size, room_count);

    let mut world = World::new();

    rooms.iter().for_each(|room| {
        let cuboid = ShapeHandle::new(
            Cuboid::new(
                Vector2::new(
                    (room.x2 - room.x1) as f32, 
                    (room.y2 - room.y1) as f32)));

        let collider_desc = ColliderDesc::new(cuboid)
            .density(1.0);
            // .translation(Vector2::new(room.x1 as f32, room.y1 as f32));
            // .build(&mut world);

        let rigid_body = RigidBodyDesc::new()
            .translation(Vector2::new(room.x1 as f32, room.y1 as f32))
            .mass(1.0)
            .collider(&collider_desc)
            .build(&mut world);
    });

    for _ in 0..5 {
        world.step();

        for collider in world.collider_world().colliders() {
            println!("{:?}\t{:?}", collider.handle(), collider.position().translation);
        }
    }
}